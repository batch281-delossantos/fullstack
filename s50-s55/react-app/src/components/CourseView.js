import {Row, Col, Button, Card} from 'react-bootstrap';
//the useParams allows us to get or extract the parameter included in our pages
import { useParams } from 'react-router-dom';

import {useEffect, useState} from 'react';

export default function CourseView(){

	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');


	const {id} = useParams();
	/*console.log(id);*/

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${id}`)
		.then(response => response.json())
		.then(data => {
			// console.log(data);
				setName(data.name);
				setPrice(data.price);
				setDescription(data.description);

		})



	}, [])


	return(
		<Row>
			<Col>
				<Card>
				    <Card.Body>
				      	<Card.Title>{name}</Card.Title>
				        <Card.Text>
				          {description}
				        </Card.Text>
				        <Card.Text>
				          Price: {price}
				        </Card.Text>
				        <Button variant="primary">Go somewhere</Button>
				      </Card.Body>
				 </Card>
			</Col>
		</Row>
		)
}