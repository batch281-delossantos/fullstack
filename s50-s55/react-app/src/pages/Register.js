// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import {Button, Form, Row, Col} from 'react-bootstrap';
//we need to import the useState from the react
import {useState, useEffect, useContext} from 'react';

import {useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import PageNotFound from './PageNotFound';
import Swal2 from 'sweetalert2';
export default function Register(){
	//State hooks to store the values of the input fields

	const [fName, setfName] = useState('');
	const [lName, setlName] = useState('');
	const [mobileNo, setMobile] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [isPassed, setIsPassed] = useState(true);
	const [isMobilePassed, setIsMobilePassed] = useState(true);

	const [isDisabled, setIsDisabled] = useState(true);

	//we are going to add/create a state that will declare whether the password1 and password 2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true); 
 
 	//when the email changes it will have a side effect that will console its value
	useEffect(()=>{
		if(email.length > 15){
			setIsPassed(false);
		}else{
			setIsPassed(true);
		}
	}, [email]);

	useEffect(()=>{
		if(mobileNo.length < 11){
			setIsMobilePassed(false);
		}else{
			setIsMobilePassed(true);
		}
	}, [mobileNo]);
	//this useEffect will disable or enable our sign up button
	useEffect(()=> {
		//we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button.
		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && email.length <= 15){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [email, password1, password2]);

	//function to simulate user registration
	function registerUser(event) {
		//prevent page reloading
		event.preventDefault();

		// console.log(mobile);
		// alert('Thank you for registering!');

		// navigate('/login');


		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		            method: 'POST',
		            headers: {
		                'Content-Type' : 'application/json'
		            },
		            body: JSON.stringify({
		                email: email,
		            })
		        })
		.then(response => response.json())
		.then(data=> {
		   
		    //if statement to check whether the email already exist.
		    if(data === true){

		         Swal2.fire({
		            title: "Duplicate Email found!",
		            icon: 'error',
		            text: 'Please provide a different email'
		        })

		    }else{
		        
		    	fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
		    	            method: 'POST',
		    	            headers: {
		    	                'Content-Type' : 'application/json'
		    	            },
		    	            body: JSON.stringify({
		    	                email: email,
		    	                firstName: fName,
		    	                lastName: lName,
		    	                mobileNo: mobileNo,
		    	                password: password1
		    	            })
		    	        })
		    	.then(response => response.json())
		    	.then(data=> {
		    	    // console.log(data);
		    	    // localStorage.getItem('token', data);

		    	    //if statement to check whether the login is successful.
		    	    if(data === false){

		    	        
		    	        /*alert('Login unsuccessful!');*/
		    	        //in adding sweetlaert2 you have to use the fire method
		    	        Swal2.fire({
		    	            title: "Login unsuccessful!",
		    	            icon: 'error',
		    	            text: 'Check your login credentials and try again'
		    	        })

		    	    }else{
		    	        

		    	        Swal2.fire({
		    	            title: 'Registration successful!',
		    	            icon: 'success',
		    	            text: 'Welcome to Zuitt!'
		    	        })

		    	        navigate('/login');

		    	    }

		    	})

		    }

		})


		
		
	}

	//useEffect to validate whether the password1 is equal to password2
	useEffect(() => {
		if(password1 !== password2){
			setIsPasswordMatch(false);
		}else{
			setIsPasswordMatch(true);
		}

	}, [password1, password2]);

	return(
		user.id === null || user.id === undefined
		?
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Register</h1>
				<Form onSubmit ={event => registerUser(event)}>

					<Form.Group className="mb-3" controlId="formBasicFname">
					  <Form.Label>First Name</Form.Label>
					  <Form.Control 
					  	type="Text" 
					  	placeholder="Enter first name" 
					  	value = {fName}
					  	onChange = {event => setfName(event.target.value)}
					  	/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="formBasicLname">
					  <Form.Label>Last Name</Form.Label>
					  <Form.Control 
					  	type="Text" 
					  	placeholder="Enter last name" 
					  	value = {lName}
					  	onChange = {event => setlName(event.target.value)}
					  	/>
					</Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="Enter email" 
				        	value = {email}
				        	onChange = {event => setEmail(event.target.value)}
				        	/>
				        <Form.Text className="text-danger" hidden = {isPassed}>
				          The email should not exceed 15 characters!
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicMobile">
				        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control 
				        	type="Text" 
				        	placeholder="Enter mobile number" 
				        	value = {mobileNo}
				        	onChange = {event => setMobile(event.target.value)}
				        	/>
				        	<Form.Text className="text-danger" hidden = {isMobilePassed}>
				        	  Mobile Number should be at leat 11 digits!
				        	</Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password" 
				        	value = {password1}
				        	onChange = {event => setPassword1(event.target.value)}
				        	/>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Retype your nominated password" 
				        	value = {password2}
				        	onChange = {event => setPassword2(event.target.value)}
				        	/>

				        <Form.Text className="text-danger" hidden = {isPasswordMatch}>
				          The passwords does not match!
				        </Form.Text>
				      </Form.Group>

				      

				      <Button variant="primary" type="submit" disabled = {isDisabled}>
				        Sign up
				      </Button>
				</Form>
			</Col>
		</Row>
		:
		<PageNotFound/>
		)
}