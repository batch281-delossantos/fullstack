import { Link, Navigate} from "react-router-dom";

export default function Undefined() {
    return (
        <div>
            <h1>Page Not Found.</h1>
            <p>Go back to the <Link to='/Home'>homepage</Link>.</p>
            
            
        </div>
    )
}