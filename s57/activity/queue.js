let collection = [];

// Write the queue functions below.

function print() {
    
    return collection;

}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    collection[collection.length] = element;
    return  collection;

   
}

function dequeue() {
    // In here you are going to remove the first element in the array

    let newArr = [];
    for(let x = 0 ; x < collection.length;x++){
        if(x!=0){

                newArr[newArr.length] = collection[x];

        }
    }

    collection = newArr;
    return collection;
}

function front() {
    // you will get the first element

     for(let x = 0 ; x < collection.length;x++){
        if(x==0){

                return collection[x];

        }
    }
}


function size() {
     // Number of elements   

      let arrayLength = 0;
      for(i in collection){

          arrayLength++;
      }
      
      let count = 0;
      for(let x = 0 ; x < arrayLength;x++){
        count++;
      }
      return count;
}

function isEmpty() {
    //it will check whether the function is empty or not

    let arrayLength = 0;
    for(i in collection){

        arrayLength++;
    }

     let count = 0;
      for(let x = 0 ; x < arrayLength;x++){
        count++;
      }
     
     if(count==0){
        return true;
     }
     else{
        return false;
     }

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};